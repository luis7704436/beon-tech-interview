import { useContactEdit } from "@hooks/contacts/useContactEdit"
import { useEffect, useState } from "react"

export const ContactForm: React.FC = () => {
  const [updatedContact, setUpdatedContact] = useState({})
  const { contact, update } = useContactEdit('507c2612-7d36-4d64-bea8-91f30d368877')

  return (
    <form>
      <input name="firstName" value={contact.firstName} placeholder="Your First Name"></input>
      <input name="lastName" value={contact.lastName} placeholder="Your Last Name"></input>
      <input name="email" value={contact.email} placeholder="email@email.com"></input>
      {/* <button onClick={() => update(updatedContact)}>Submit the </button> */}
    </form>
  )
}